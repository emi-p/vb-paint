﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formSuperPaint
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(formSuperPaint))
        Me.pbImagen = New System.Windows.Forms.PictureBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.stEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.msPaint = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSMIabrir = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSMInuevo = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSMIguardar = New System.Windows.Forms.ToolStripMenuItem()
        Me.TMSIsalir = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblBlack = New System.Windows.Forms.Label()
        Me.lblNavy = New System.Windows.Forms.Label()
        Me.lblGreen = New System.Windows.Forms.Label()
        Me.lblTeal = New System.Windows.Forms.Label()
        Me.lblMaroon = New System.Windows.Forms.Label()
        Me.lblPurple = New System.Windows.Forms.Label()
        Me.lblCyan = New System.Windows.Forms.Label()
        Me.lblLime = New System.Windows.Forms.Label()
        Me.lblBlue = New System.Windows.Forms.Label()
        Me.lblGray = New System.Windows.Forms.Label()
        Me.lblSilver = New System.Windows.Forms.Label()
        Me.lblOlive = New System.Windows.Forms.Label()
        Me.lblWhite = New System.Windows.Forms.Label()
        Me.lblYellow = New System.Windows.Forms.Label()
        Me.lblFuchsia = New System.Windows.Forms.Label()
        Me.lblRed = New System.Windows.Forms.Label()
        Me.lblColorSeleccionado = New System.Windows.Forms.Label()
        Me.lblBrush = New System.Windows.Forms.Label()
        Me.lblLine = New System.Windows.Forms.Label()
        Me.lblCuadrado = New System.Windows.Forms.Label()
        Me.lblCuadradoRelleno = New System.Windows.Forms.Label()
        Me.lblCirculo = New System.Windows.Forms.Label()
        Me.lblCirculoRelleno = New System.Windows.Forms.Label()
        Me.lblElipse = New System.Windows.Forms.Label()
        Me.lblElipseRellena = New System.Windows.Forms.Label()
        Me.cbTamañoPincel = New System.Windows.Forms.ComboBox()
        Me.cbAnchoLapiz = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.sfdGuardarBitMap = New System.Windows.Forms.SaveFileDialog()
        CType(Me.pbImagen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.msPaint.SuspendLayout()
        Me.SuspendLayout()
        '
        'pbImagen
        '
        Me.pbImagen.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbImagen.BackColor = System.Drawing.Color.DarkGray
        Me.pbImagen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pbImagen.Cursor = System.Windows.Forms.Cursors.Default
        Me.pbImagen.Enabled = False
        Me.pbImagen.Location = New System.Drawing.Point(166, 33)
        Me.pbImagen.Name = "pbImagen"
        Me.pbImagen.Size = New System.Drawing.Size(883, 569)
        Me.pbImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbImagen.TabIndex = 0
        Me.pbImagen.TabStop = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.DefaultExt = "*.jpg *.png"
        Me.OpenFileDialog1.Filter = "Imagenes |*jpg"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.stEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 605)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1061, 22)
        Me.StatusStrip1.TabIndex = 9
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'stEstado
        '
        Me.stEstado.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stEstado.Name = "stEstado"
        Me.stEstado.Size = New System.Drawing.Size(35, 17)
        Me.stEstado.Text = "Listo"
        '
        'msPaint
        '
        Me.msPaint.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.msPaint.Dock = System.Windows.Forms.DockStyle.None
        Me.msPaint.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem})
        Me.msPaint.Location = New System.Drawing.Point(0, 0)
        Me.msPaint.Name = "msPaint"
        Me.msPaint.Size = New System.Drawing.Size(83, 29)
        Me.msPaint.TabIndex = 10
        Me.msPaint.Text = "MenuStrip1"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSMIabrir, Me.TSMInuevo, Me.TSMIguardar, Me.TMSIsalir})
        Me.ArchivoToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(75, 25)
        Me.ArchivoToolStripMenuItem.Text = "Archivo"
        '
        'TSMIabrir
        '
        Me.TSMIabrir.Name = "TSMIabrir"
        Me.TSMIabrir.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.TSMIabrir.Size = New System.Drawing.Size(194, 26)
        Me.TSMIabrir.Text = "Abrir"
        '
        'TSMInuevo
        '
        Me.TSMInuevo.Name = "TSMInuevo"
        Me.TSMInuevo.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.TSMInuevo.Size = New System.Drawing.Size(194, 26)
        Me.TSMInuevo.Text = "Nuevo"
        '
        'TSMIguardar
        '
        Me.TSMIguardar.Enabled = False
        Me.TSMIguardar.Name = "TSMIguardar"
        Me.TSMIguardar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.TSMIguardar.Size = New System.Drawing.Size(194, 26)
        Me.TSMIguardar.Text = "Guardar"
        '
        'TMSIsalir
        '
        Me.TMSIsalir.Name = "TMSIsalir"
        Me.TMSIsalir.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.TMSIsalir.Size = New System.Drawing.Size(194, 26)
        Me.TMSIsalir.Text = "Salir"
        '
        'lblBlack
        '
        Me.lblBlack.BackColor = System.Drawing.Color.Black
        Me.lblBlack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBlack.Location = New System.Drawing.Point(23, 83)
        Me.lblBlack.Name = "lblBlack"
        Me.lblBlack.Size = New System.Drawing.Size(23, 23)
        Me.lblBlack.TabIndex = 11
        '
        'lblNavy
        '
        Me.lblNavy.BackColor = System.Drawing.Color.Navy
        Me.lblNavy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNavy.Location = New System.Drawing.Point(52, 83)
        Me.lblNavy.Name = "lblNavy"
        Me.lblNavy.Size = New System.Drawing.Size(23, 23)
        Me.lblNavy.TabIndex = 12
        '
        'lblGreen
        '
        Me.lblGreen.BackColor = System.Drawing.Color.Green
        Me.lblGreen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblGreen.Location = New System.Drawing.Point(81, 83)
        Me.lblGreen.Name = "lblGreen"
        Me.lblGreen.Size = New System.Drawing.Size(23, 23)
        Me.lblGreen.TabIndex = 13
        '
        'lblTeal
        '
        Me.lblTeal.BackColor = System.Drawing.Color.Teal
        Me.lblTeal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTeal.Location = New System.Drawing.Point(23, 115)
        Me.lblTeal.Name = "lblTeal"
        Me.lblTeal.Size = New System.Drawing.Size(23, 23)
        Me.lblTeal.TabIndex = 14
        '
        'lblMaroon
        '
        Me.lblMaroon.BackColor = System.Drawing.Color.Maroon
        Me.lblMaroon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMaroon.Location = New System.Drawing.Point(52, 115)
        Me.lblMaroon.Name = "lblMaroon"
        Me.lblMaroon.Size = New System.Drawing.Size(23, 23)
        Me.lblMaroon.TabIndex = 15
        '
        'lblPurple
        '
        Me.lblPurple.BackColor = System.Drawing.Color.Purple
        Me.lblPurple.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPurple.Location = New System.Drawing.Point(81, 115)
        Me.lblPurple.Name = "lblPurple"
        Me.lblPurple.Size = New System.Drawing.Size(23, 23)
        Me.lblPurple.TabIndex = 16
        '
        'lblCyan
        '
        Me.lblCyan.BackColor = System.Drawing.Color.Cyan
        Me.lblCyan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCyan.Location = New System.Drawing.Point(81, 179)
        Me.lblCyan.Name = "lblCyan"
        Me.lblCyan.Size = New System.Drawing.Size(23, 23)
        Me.lblCyan.TabIndex = 22
        '
        'lblLime
        '
        Me.lblLime.BackColor = System.Drawing.Color.Lime
        Me.lblLime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLime.Location = New System.Drawing.Point(52, 179)
        Me.lblLime.Name = "lblLime"
        Me.lblLime.Size = New System.Drawing.Size(23, 23)
        Me.lblLime.TabIndex = 21
        '
        'lblBlue
        '
        Me.lblBlue.BackColor = System.Drawing.Color.Blue
        Me.lblBlue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBlue.Location = New System.Drawing.Point(23, 179)
        Me.lblBlue.Name = "lblBlue"
        Me.lblBlue.Size = New System.Drawing.Size(23, 23)
        Me.lblBlue.TabIndex = 20
        '
        'lblGray
        '
        Me.lblGray.BackColor = System.Drawing.Color.Gray
        Me.lblGray.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblGray.Location = New System.Drawing.Point(81, 147)
        Me.lblGray.Name = "lblGray"
        Me.lblGray.Size = New System.Drawing.Size(23, 23)
        Me.lblGray.TabIndex = 19
        '
        'lblSilver
        '
        Me.lblSilver.BackColor = System.Drawing.Color.Silver
        Me.lblSilver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSilver.Location = New System.Drawing.Point(52, 147)
        Me.lblSilver.Name = "lblSilver"
        Me.lblSilver.Size = New System.Drawing.Size(23, 23)
        Me.lblSilver.TabIndex = 18
        '
        'lblOlive
        '
        Me.lblOlive.BackColor = System.Drawing.Color.Olive
        Me.lblOlive.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblOlive.Location = New System.Drawing.Point(23, 147)
        Me.lblOlive.Name = "lblOlive"
        Me.lblOlive.Size = New System.Drawing.Size(23, 23)
        Me.lblOlive.TabIndex = 17
        '
        'lblWhite
        '
        Me.lblWhite.BackColor = System.Drawing.Color.White
        Me.lblWhite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWhite.Location = New System.Drawing.Point(110, 179)
        Me.lblWhite.Name = "lblWhite"
        Me.lblWhite.Size = New System.Drawing.Size(23, 23)
        Me.lblWhite.TabIndex = 27
        '
        'lblYellow
        '
        Me.lblYellow.BackColor = System.Drawing.Color.Yellow
        Me.lblYellow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblYellow.Location = New System.Drawing.Point(110, 147)
        Me.lblYellow.Name = "lblYellow"
        Me.lblYellow.Size = New System.Drawing.Size(23, 23)
        Me.lblYellow.TabIndex = 26
        '
        'lblFuchsia
        '
        Me.lblFuchsia.BackColor = System.Drawing.Color.Fuchsia
        Me.lblFuchsia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFuchsia.Location = New System.Drawing.Point(110, 115)
        Me.lblFuchsia.Name = "lblFuchsia"
        Me.lblFuchsia.Size = New System.Drawing.Size(23, 23)
        Me.lblFuchsia.TabIndex = 25
        '
        'lblRed
        '
        Me.lblRed.BackColor = System.Drawing.Color.Red
        Me.lblRed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRed.Location = New System.Drawing.Point(110, 83)
        Me.lblRed.Name = "lblRed"
        Me.lblRed.Size = New System.Drawing.Size(23, 23)
        Me.lblRed.TabIndex = 24
        '
        'lblColorSeleccionado
        '
        Me.lblColorSeleccionado.BackColor = System.Drawing.Color.Black
        Me.lblColorSeleccionado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblColorSeleccionado.Location = New System.Drawing.Point(23, 214)
        Me.lblColorSeleccionado.Name = "lblColorSeleccionado"
        Me.lblColorSeleccionado.Size = New System.Drawing.Size(110, 23)
        Me.lblColorSeleccionado.TabIndex = 28
        '
        'lblBrush
        '
        Me.lblBrush.BackColor = System.Drawing.Color.Transparent
        Me.lblBrush.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBrush.Image = CType(resources.GetObject("lblBrush.Image"), System.Drawing.Image)
        Me.lblBrush.Location = New System.Drawing.Point(34, 266)
        Me.lblBrush.Name = "lblBrush"
        Me.lblBrush.Size = New System.Drawing.Size(34, 34)
        Me.lblBrush.TabIndex = 29
        '
        'lblLine
        '
        Me.lblLine.BackColor = System.Drawing.Color.Transparent
        Me.lblLine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLine.Image = CType(resources.GetObject("lblLine.Image"), System.Drawing.Image)
        Me.lblLine.Location = New System.Drawing.Point(81, 266)
        Me.lblLine.Name = "lblLine"
        Me.lblLine.Size = New System.Drawing.Size(34, 34)
        Me.lblLine.TabIndex = 30
        '
        'lblCuadrado
        '
        Me.lblCuadrado.BackColor = System.Drawing.Color.Transparent
        Me.lblCuadrado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCuadrado.Image = CType(resources.GetObject("lblCuadrado.Image"), System.Drawing.Image)
        Me.lblCuadrado.Location = New System.Drawing.Point(34, 312)
        Me.lblCuadrado.Name = "lblCuadrado"
        Me.lblCuadrado.Size = New System.Drawing.Size(34, 34)
        Me.lblCuadrado.TabIndex = 31
        '
        'lblCuadradoRelleno
        '
        Me.lblCuadradoRelleno.BackColor = System.Drawing.Color.Transparent
        Me.lblCuadradoRelleno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCuadradoRelleno.Image = CType(resources.GetObject("lblCuadradoRelleno.Image"), System.Drawing.Image)
        Me.lblCuadradoRelleno.Location = New System.Drawing.Point(81, 312)
        Me.lblCuadradoRelleno.Name = "lblCuadradoRelleno"
        Me.lblCuadradoRelleno.Size = New System.Drawing.Size(34, 34)
        Me.lblCuadradoRelleno.TabIndex = 32
        '
        'lblCirculo
        '
        Me.lblCirculo.BackColor = System.Drawing.Color.Transparent
        Me.lblCirculo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCirculo.Image = CType(resources.GetObject("lblCirculo.Image"), System.Drawing.Image)
        Me.lblCirculo.Location = New System.Drawing.Point(34, 357)
        Me.lblCirculo.Name = "lblCirculo"
        Me.lblCirculo.Size = New System.Drawing.Size(34, 34)
        Me.lblCirculo.TabIndex = 33
        '
        'lblCirculoRelleno
        '
        Me.lblCirculoRelleno.BackColor = System.Drawing.Color.Transparent
        Me.lblCirculoRelleno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCirculoRelleno.Image = CType(resources.GetObject("lblCirculoRelleno.Image"), System.Drawing.Image)
        Me.lblCirculoRelleno.Location = New System.Drawing.Point(81, 357)
        Me.lblCirculoRelleno.Name = "lblCirculoRelleno"
        Me.lblCirculoRelleno.Size = New System.Drawing.Size(34, 34)
        Me.lblCirculoRelleno.TabIndex = 34
        '
        'lblElipse
        '
        Me.lblElipse.BackColor = System.Drawing.Color.Transparent
        Me.lblElipse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblElipse.Image = CType(resources.GetObject("lblElipse.Image"), System.Drawing.Image)
        Me.lblElipse.Location = New System.Drawing.Point(34, 404)
        Me.lblElipse.Name = "lblElipse"
        Me.lblElipse.Size = New System.Drawing.Size(34, 34)
        Me.lblElipse.TabIndex = 35
        '
        'lblElipseRellena
        '
        Me.lblElipseRellena.BackColor = System.Drawing.Color.Transparent
        Me.lblElipseRellena.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblElipseRellena.Image = CType(resources.GetObject("lblElipseRellena.Image"), System.Drawing.Image)
        Me.lblElipseRellena.Location = New System.Drawing.Point(81, 404)
        Me.lblElipseRellena.Name = "lblElipseRellena"
        Me.lblElipseRellena.Size = New System.Drawing.Size(34, 34)
        Me.lblElipseRellena.TabIndex = 36
        '
        'cbTamañoPincel
        '
        Me.cbTamañoPincel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTamañoPincel.FormattingEnabled = True
        Me.cbTamañoPincel.Items.AddRange(New Object() {"1", "2", "4", "6", "8", "10", "12", "14", "16", "18", "20"})
        Me.cbTamañoPincel.Location = New System.Drawing.Point(12, 484)
        Me.cbTamañoPincel.Name = "cbTamañoPincel"
        Me.cbTamañoPincel.Size = New System.Drawing.Size(148, 28)
        Me.cbTamañoPincel.TabIndex = 37
        '
        'cbAnchoLapiz
        '
        Me.cbAnchoLapiz.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAnchoLapiz.FormattingEnabled = True
        Me.cbAnchoLapiz.Items.AddRange(New Object() {"1", "2", "4", "6", "8", "10"})
        Me.cbAnchoLapiz.Location = New System.Drawing.Point(12, 552)
        Me.cbAnchoLapiz.Name = "cbAnchoLapiz"
        Me.cbAnchoLapiz.Size = New System.Drawing.Size(148, 28)
        Me.cbAnchoLapiz.TabIndex = 38
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 460)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(148, 21)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "Tamaño del Pincel"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 526)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(148, 23)
        Me.Label2.TabIndex = 40
        Me.Label2.Text = "Grosor del boligrafo"
        '
        'formSuperPaint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1061, 627)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbAnchoLapiz)
        Me.Controls.Add(Me.cbTamañoPincel)
        Me.Controls.Add(Me.lblElipseRellena)
        Me.Controls.Add(Me.lblElipse)
        Me.Controls.Add(Me.lblCirculoRelleno)
        Me.Controls.Add(Me.lblCirculo)
        Me.Controls.Add(Me.lblCuadradoRelleno)
        Me.Controls.Add(Me.lblCuadrado)
        Me.Controls.Add(Me.lblLine)
        Me.Controls.Add(Me.lblBrush)
        Me.Controls.Add(Me.lblColorSeleccionado)
        Me.Controls.Add(Me.lblWhite)
        Me.Controls.Add(Me.lblYellow)
        Me.Controls.Add(Me.lblFuchsia)
        Me.Controls.Add(Me.lblRed)
        Me.Controls.Add(Me.lblCyan)
        Me.Controls.Add(Me.lblLime)
        Me.Controls.Add(Me.lblBlue)
        Me.Controls.Add(Me.lblGray)
        Me.Controls.Add(Me.lblSilver)
        Me.Controls.Add(Me.lblOlive)
        Me.Controls.Add(Me.lblPurple)
        Me.Controls.Add(Me.lblMaroon)
        Me.Controls.Add(Me.lblTeal)
        Me.Controls.Add(Me.lblGreen)
        Me.Controls.Add(Me.lblNavy)
        Me.Controls.Add(Me.lblBlack)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.msPaint)
        Me.Controls.Add(Me.pbImagen)
        Me.MainMenuStrip = Me.msPaint
        Me.Name = "formSuperPaint"
        Me.Text = "Super Paint"
        CType(Me.pbImagen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.msPaint.ResumeLayout(False)
        Me.msPaint.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pbImagen As PictureBox
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents stEstado As ToolStripStatusLabel
    Friend WithEvents msPaint As MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TSMIabrir As ToolStripMenuItem
    Friend WithEvents TSMInuevo As ToolStripMenuItem
    Friend WithEvents TSMIguardar As ToolStripMenuItem
    Friend WithEvents TMSIsalir As ToolStripMenuItem
    Friend WithEvents lblBlack As Label
    Friend WithEvents lblNavy As Label
    Friend WithEvents lblGreen As Label
    Friend WithEvents lblTeal As Label
    Friend WithEvents lblMaroon As Label
    Friend WithEvents lblPurple As Label
    Friend WithEvents lblCyan As Label
    Friend WithEvents lblLime As Label
    Friend WithEvents lblBlue As Label
    Friend WithEvents lblGray As Label
    Friend WithEvents lblSilver As Label
    Friend WithEvents lblOlive As Label
    Friend WithEvents lblWhite As Label
    Friend WithEvents lblYellow As Label
    Friend WithEvents lblFuchsia As Label
    Friend WithEvents lblRed As Label
    Friend WithEvents lblColorSeleccionado As Label
    Friend WithEvents lblBrush As Label
    Friend WithEvents lblLine As Label
    Friend WithEvents lblCuadrado As Label
    Friend WithEvents lblCuadradoRelleno As Label
    Friend WithEvents lblCirculo As Label
    Friend WithEvents lblCirculoRelleno As Label
    Friend WithEvents lblElipse As Label
    Friend WithEvents lblElipseRellena As Label
    Friend WithEvents cbTamañoPincel As ComboBox
    Friend WithEvents cbAnchoLapiz As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents sfdGuardarBitMap As SaveFileDialog
End Class
