﻿Public Class formSuperPaint
    Dim mbFoto As Bitmap
    Dim gHerramientaDibujo As Graphics
    Dim banderaDibujar As Boolean = False
    Dim xAbajo, yAbajo, xArriba, yArriba As Integer
    Dim iHerramientaSeleccionada As Byte
    Dim cColorSeleccionado As Color = Color.Black
    Dim iTamañoPincel As Byte = 6
    Dim iAnchoLapicera As Byte = 2
    Dim iL, iR, iT, iB, iW, iH As Integer

    Public Sub resetTools()
        lblBrush.BorderStyle = BorderStyle.Fixed3D
        lblLine.BorderStyle = BorderStyle.Fixed3D
        lblCirculo.BorderStyle = BorderStyle.Fixed3D
        lblCirculoRelleno.BorderStyle = BorderStyle.Fixed3D
        lblCuadrado.BorderStyle = BorderStyle.Fixed3D
        lblCuadradoRelleno.BorderStyle = BorderStyle.Fixed3D
        lblElipse.BorderStyle = BorderStyle.Fixed3D
        lblElipseRellena.BorderStyle = BorderStyle.Fixed3D
    End Sub
    Public Sub dimensionCuadrado()
        If xArriba < 0 Then xArriba = 0
        If xArriba > 883 Then xArriba = 883
        If yArriba < 0 Then yArriba = 0
        If yArriba > 569 Then yArriba = 569
        iW = Math.Abs(xArriba - xAbajo)
        iH = Math.Abs(yArriba - yAbajo)
        If iW > iH Then iW = iH
        If xArriba < xAbajo Then iL = xAbajo - iW Else iL = xAbajo
        If yArriba < yAbajo Then iT = yAbajo - iW Else iT = yAbajo
    End Sub
    Private Sub btnOpenImagen_Click(sender As Object, e As EventArgs)
        OpenFileDialog1.ShowDialog()
        Dim ruta As String = "No seleccionaste ninguna imagen"
        If OpenFileDialog1.FileName <> "" Then
            ruta = OpenFileDialog1.FileName.ToString
            pbImagen.Image = Image.FromFile(ruta)
            stEstado.Text = ruta
        Else
            MsgBox(ruta)
        End If
    End Sub

    Private Sub AbrirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TSMIabrir.Click
        OpenFileDialog1.ShowDialog()
        Dim ruta As String = "No seleccionaste ninguna imagen"
        If OpenFileDialog1.FileName <> "" Then
            pbImagen.Enabled = True
            TSMIguardar.Enabled = True
            ruta = OpenFileDialog1.FileName.ToString
            pbImagen.Image = Image.FromFile(ruta)
            stEstado.Text = ruta
        Else
            MsgBox(ruta)
        End If
    End Sub

    Private Sub TSMInuevo_Click(sender As Object, e As EventArgs) Handles TSMInuevo.Click
        TSMIguardar.Enabled = True
        pbImagen.Enabled = True
        pbImagen.BackColor = Color.White
        mbFoto = New Bitmap(pbImagen.Width, pbImagen.Height)
        pbImagen.Image = mbFoto
        gHerramientaDibujo = Graphics.FromImage(mbFoto)
        pbImagen.DrawToBitmap(mbFoto, pbImagen.ClientRectangle)
    End Sub

    Private Sub TSMIguardar_Click(sender As Object, e As EventArgs) Handles TSMIguardar.Click
        sfdGuardarBitMap.Filter = "Bitmap | * .bmp"
        If sfdGuardarBitMap.ShowDialog = DialogResult.OK Then
            pbImagen.Image.Save(sfdGuardarBitMap.FileName, Drawing.Imaging.ImageFormat.Bmp)
            MsgBox("Archivo guardado")
        End If
    End Sub
    Private Sub lblBlack_Click(sender As Object, e As EventArgs) Handles lblBlack.Click, lblBlue.Click, lblCyan.Click,
            lblFuchsia.Click, lblGray.Click, lblGreen.Click, lblLime.Click, lblMaroon.Click, lblNavy.Click,
            lblOlive.Click, lblPurple.Click, lblRed.Click, lblSilver.Click, lblTeal.Click, lblWhite.Click, lblYellow.Click
        lblColorSeleccionado.BackColor = sender.BackColor
        cColorSeleccionado = sender.BackColor
        resetTools()
    End Sub


    Private Sub cbTamañoPincel_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbTamañoPincel.SelectedValueChanged
        iTamañoPincel = Convert.ToByte(cbTamañoPincel.Text)
    End Sub

    Private Sub lblBrush_Click(sender As Object, e As EventArgs) Handles lblBrush.Click
        iHerramientaSeleccionada = 1
    End Sub
    Private Sub lblLine_Click(sender As Object, e As EventArgs) Handles lblLine.Click
        iHerramientaSeleccionada = 2
    End Sub
    Private Sub lblCuadrado_Click(sender As Object, e As EventArgs) Handles lblCuadrado.Click
        iHerramientaSeleccionada = 3
    End Sub
    Private Sub lblCuadradoRelleno_Click(sender As Object, e As EventArgs) Handles lblCuadradoRelleno.Click
        iHerramientaSeleccionada = 4
    End Sub


    Private Sub lblCirculo_Click(sender As Object, e As EventArgs) Handles lblCirculo.Click
        iHerramientaSeleccionada = 5
    End Sub
    Private Sub lblCirculoRelleno_Click(sender As Object, e As EventArgs) Handles lblCirculoRelleno.Click
        iHerramientaSeleccionada = 6
    End Sub
    Private Sub lblElipse_Click(sender As Object, e As EventArgs) Handles lblElipse.Click
        iHerramientaSeleccionada = 7
    End Sub
    Private Sub lblElipseRellena_Click(sender As Object, e As EventArgs) Handles lblElipseRellena.Click
        iHerramientaSeleccionada = 8
    End Sub
    Private Sub cbAnchoLapiz_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbAnchoLapiz.SelectedValueChanged
        Dim iTamañoLapiz As Byte
        iTamañoLapiz = Convert.ToByte(cbAnchoLapiz.Text)
    End Sub

    Private Sub pbImagen_MouseDown(sender As Object, e As MouseEventArgs) Handles pbImagen.MouseDown
        banderaDibujar = True
        xAbajo = e.X
        yAbajo = e.Y
    End Sub

    Private Sub pbImagen_MouseMove(sender As Object, e As MouseEventArgs) Handles pbImagen.MouseMove
        If iHerramientaSeleccionada = 1 And banderaDibujar = True Then
            xAbajo = e.X
            yAbajo = e.Y
            gHerramientaDibujo.FillEllipse(New SolidBrush(cColorSeleccionado), xAbajo, yAbajo, iTamañoPincel, iTamañoPincel)
            pbImagen.Refresh()
        End If
    End Sub

    Private Sub pbImagen_MouseUp(sender As Object, e As MouseEventArgs) Handles pbImagen.MouseUp
        banderaDibujar = False
        Dim pincelRelleno As SolidBrush = New Drawing.SolidBrush(cColorSeleccionado)
        Dim pLapicera As New Pen(cColorSeleccionado, iAnchoLapicera)
        xArriba = e.X
        yArriba = e.Y

        Select Case iHerramientaSeleccionada
            Case 2
                gHerramientaDibujo.DrawLine(pLapicera, xAbajo, yAbajo, xArriba, yArriba)
                pbImagen.Refresh()
            Case 3
                gHerramientaDibujo.DrawRectangle(pLapicera, xAbajo, yAbajo, xArriba, yArriba)
                pbImagen.Refresh()
            Case 4
                dimensionCuadrado()
                gHerramientaDibujo.FillRectangle(pincelRelleno, iL, iT, iW, iW)
                pbImagen.Refresh()
            Case 5
                gHerramientaDibujo.DrawEllipse(pLapicera, xAbajo, yAbajo, xArriba, yArriba)
                pbImagen.Refresh()
            Case 6
                gHerramientaDibujo.FillEllipse(pincelRelleno, iL, iT, iW, iW)
                pbImagen.Refresh()

        End Select

    End Sub
End Class
